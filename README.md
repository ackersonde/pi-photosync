[![pipeline status](https://gitlab.com/ackersonde/pi-photosync/badges/master/pipeline.svg)](https://gitlab.com/ackersonde/pi-photosync/-/commits/master)

# pi-photosync
This repo deploys [Photoprism](https://docs.photoprism.app/getting-started/) and [Syncthing](https://docs.syncthing.net/intro/getting-started.html#getting-started) on my home network for photo management.
