#!/bin/bash

# Setup photoprism
mkdir -p /home/ackersond/photoprism/storage
mv backup_photos.sh import_photos.sh docker-compose.yml /home/ackersond/photoprism/

# Setup Syncthing config
mkdir -p /home/ackersond/Pictures /home/ackersond/syncthing/config /home/ackersond/syncthing/Camera /home/ackersond/syncthing/ipadPhotos
ln -s /photoprism/syncthing/ipadPhotos /home/ackersond/Pictures/ipadPhotos
sudo chown -Rf ackersond:ackersond /home/ackersond/syncthing
echo ".trashed-*" > /home/ackersond/syncthing/Camera/.stignore
echo ".pending-*" >> /home/ackersond/syncthing/Camera/.stignore
chmod 600 /home/ackersond/syncthing/Camera/.stignore
echo -n "$SYNCTHING_CERT_B64" | base64 -d | tee /home/ackersond/syncthing/config/cert.pem >/dev/null
chmod 644 /home/ackersond/syncthing/config/cert.pem
echo -n "$SYNCTHING_KEY_B64" | base64 -d | tee /home/ackersond/syncthing/config/key.pem >/dev/null
chmod 600 /home/ackersond/syncthing/config/key.pem
echo -n "$SYNCTHING_CONFIG_B64" | base64 -d | tee /home/ackersond/syncthing/config/config.xml >/dev/null
chmod 600 /home/ackersond/syncthing/config/config.xml
